import TablasBBDD.*
import java.sql.Date
fun main(){

    var AdministradorBBDD:DataBaseAdmin=DataBaseAdmin()

    AdministradorBBDD.iniciarConexion("vksgpnym","ac3larzfYYfeCTwvNy4GILdtN_1y6pp6")

    /*
    Pruebas de que se pueden leer correctamente los datos de los objetos
    se imprime por consola alguna de las columnas para comprobar que coge bien los datos
     */

    for (i in AdministradorBBDD.leerAlumno()){
        print(" "+i.discapacidad+" ")
    }
    println("")

    for (i in AdministradorBBDD.leerAlumnosVsTitulaciones()){
        print(" "+i.codigoTitulacion+" ")
    }
    println("")

    for (i in AdministradorBBDD.leerTitulaciones()){
        print(" "+i.precioCredito+" ")
    }
    println("")

    for (i in AdministradorBBDD.leerTitulacionesVsAsignaturas()){
        print(" "+i.codigoAsignatura+" ")
    }
    println("")

    for (i in AdministradorBBDD.leerAsignaturas()){
        print(" "+i.profesor+" ")
    }
    println("")

    /*
    Pruebas de inserciones
     */

    var alumno:Alumno = Alumno("11111111T","Prueba","Prueba", Date(700000000000),false)
    AdministradorBBDD.insertarAlumno(alumno)

    var titulacion:Titulacion = Titulacion("TR&L-M-1","Trasportes",100,50.0)
    AdministradorBBDD.insertarTitulacion(titulacion)

    var asignatura:Asignatura = Asignatura("OFIM-M-1","Ofimatica",11,"Ines Herrera")
    AdministradorBBDD.insertarAsignatura(asignatura)

    var alumnoVsTitulacion:AlumnoVsTitulacion = AlumnoVsTitulacion(30,"DAMP-M-1","54365430K")
    AdministradorBBDD.insertarAlumnosVsTitulaciones(alumnoVsTitulacion)

    var titulacionVsAsignatura:TitulacionVsAsignatura = TitulacionVsAsignatura(30,"DAMP-M-1","CONTA-M-1")
    AdministradorBBDD.insertarTitulacionesVsAsignaturas(titulacionVsAsignatura)


}