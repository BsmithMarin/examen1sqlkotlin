import java.lang.Exception

import TablasBBDD.*
import java.sql.*

class DataBaseAdmin {

    var conn:Connection? = null

    fun iniciarConexion(usuario:String,password:String){//conexion con la base de datos remota

        try {
            val url: String = ("jdbc:postgresql://kandula.db.elephantsql.com:5432/" + usuario)
            conn = DriverManager.getConnection(url, usuario, password)
        }catch (e:Exception){

            e.printStackTrace()
        }
    }

    /*
    Todas las lecturas de datos siguen la misma estructura, por cada fila se crea un objeto que luego se añade a una lista
    de objetos para conservar los datos durante la ejecucuion del programa
     */

    fun leerAlumno():ArrayList<Alumno>{

        var arListaAlumnos = arrayListOf<Alumno>()

        var statement: Statement? = null
        var resultSet: ResultSet? = null

        try{

            statement= conn!!.createStatement()
            resultSet= statement.executeQuery("SELECT * FROM \"public\".\"Alumnos\"")

            while (resultSet.next() == true ){

                var FilaTabla: Alumno = Alumno(resultSet.getString(1),resultSet.getString(2),
                            resultSet.getString(3),resultSet.getDate(4),resultSet.getBoolean(5))
                arListaAlumnos.add(FilaTabla)

            }

        }catch (e:Exception){

            e.printStackTrace()
        }


        return arListaAlumnos
    }

    fun leerTitulaciones():ArrayList<Titulacion>{

        var arListaTitulaciones = arrayListOf<Titulacion>()

        var statement:Statement? = null
        var resultSet: ResultSet? = null

        try{

            statement= conn!!.createStatement()
            resultSet= statement.executeQuery("SELECT * FROM \"public\".\"Titulaciones\"")

            while (resultSet.next() == true ){

                var FilaTabla: Titulacion = Titulacion(resultSet.getString(1),resultSet.getString(2),
                                                        resultSet.getInt(3),resultSet.getDouble(4))
                arListaTitulaciones.add(FilaTabla)

            }

        }catch (e:Exception){

            e.printStackTrace()
        }


        return arListaTitulaciones
    }

    fun leerAsignaturas():ArrayList<Asignatura>{

        var arListaAsignaturas = arrayListOf<Asignatura>()

        var statement:Statement? = null
        var resultSet: ResultSet? = null

        try{

            statement= conn!!.createStatement()
            resultSet= statement.executeQuery("SELECT * FROM \"public\".\"Asignaturas\"")

            while (resultSet.next() == true ){

                var FilaTabla: Asignatura = Asignatura(resultSet.getString(1),resultSet.getString(2),
                                                        resultSet.getInt(3),resultSet.getString(4))
                arListaAsignaturas.add(FilaTabla)

            }

        }catch (e:Exception){

            e.printStackTrace()
        }

        return arListaAsignaturas
    }

    fun leerAlumnosVsTitulaciones():ArrayList<AlumnoVsTitulacion>{

        var arListaAlumnosVsTitulaciones = arrayListOf<AlumnoVsTitulacion>()

        var statement:Statement? = null
        var resultSet: ResultSet? = null

        try{

            statement= conn!!.createStatement()
            resultSet= statement.executeQuery("SELECT * FROM \"public\".\"AlumnosVsTitulaciones\"")

            while (resultSet.next() == true ){

                var FilaTabla: AlumnoVsTitulacion = AlumnoVsTitulacion(resultSet.getInt(1),resultSet.getString(2),
                                                                        resultSet.getString(3))
                arListaAlumnosVsTitulaciones.add(FilaTabla)

            }

        }catch (e:Exception){

            e.printStackTrace()
        }

        return arListaAlumnosVsTitulaciones
    }

    fun leerTitulacionesVsAsignaturas():ArrayList<TitulacionVsAsignatura>{

        var arListaTitulacionesVsAsignaturas = arrayListOf<TitulacionVsAsignatura>()

        var statement:Statement? = null
        var resultSet: ResultSet? = null

        try{

            statement= conn!!.createStatement()
            resultSet= statement.executeQuery("SELECT * FROM \"public\".\"TitulacionesVsAsignaturas\"")

            while (resultSet.next() == true ){

                var FilaTabla: TitulacionVsAsignatura = TitulacionVsAsignatura(resultSet.getInt(1),resultSet.getString(2),
                                                                                resultSet.getString(3))
                arListaTitulacionesVsAsignaturas.add(FilaTabla)

            }

        }catch (e:Exception){

            e.printStackTrace()
        }


        return arListaTitulacionesVsAsignaturas
    }

    /*
    Para insertar filas en la tablas se sigue siempre la misma estructura, se guardan en variables los atributos del objeto
    recibido por parametro, para que sea mas facil trabajar con ellos y quede un codigo mas corto y legible.
    Luego se ejecuta la adicion de fila metiendo los datos de las variables en el texto del UPDATE de SQL
     */

    fun insertarAlumno(alumno:Alumno){

        var statement:Statement? = null

        var nif:String? = alumno.nif
        var Nombre:String? = alumno.nombre
        var Apellidos:String? = alumno.apellidos
        var Fechanacimeinto:Date? = alumno.fechaNacimiento
        var Discapacidad:Boolean? = alumno.discapacidad

        try{
            statement= conn!!.createStatement()
            statement.executeUpdate("INSERT INTO \"public\".\"Alumnos\" VALUES ('"+nif+"','"+Nombre+"','"+Apellidos+"','"+Fechanacimeinto+"','"+Discapacidad+"')")

        }catch (e:Exception){
            e.printStackTrace()
        }
    }

    fun insertarTitulacion(titulacion:Titulacion){

        var statement:Statement? = null

        var Codigo:String? = titulacion.codigo
        var Nombre:String? = titulacion.nombre
        var Creditos:Int? = titulacion.creditos
        var PrecioCredito:Double? = titulacion.precioCredito


        try{
            statement= conn!!.createStatement()
            statement.executeUpdate("INSERT INTO \"public\".\"Titulaciones\" VALUES ('"+Codigo+"','"+Nombre+"',"+Creditos+","+PrecioCredito+")")

        }catch (e:Exception){
            e.printStackTrace()
        }
    }

    fun insertarAsignatura(asignatura:Asignatura){

        var statement:Statement? = null

        var Codigo:String? = asignatura.codigo
        var Nombre:String? = asignatura.nombre
        var Creditos:Int? = asignatura.creditos
        var Profesor:String? = asignatura.profesor


        try{
            statement= conn!!.createStatement()
            statement.executeUpdate("INSERT INTO \"public\".\"Asignaturas\" VALUES ('"+Codigo+"','"+Nombre+"',"+Creditos+",'"+Profesor+"')")

        }catch (e:Exception){
            e.printStackTrace()
        }
    }

    fun insertarAlumnosVsTitulaciones(alumnoVsTitulacion:AlumnoVsTitulacion){

        var statement:Statement? = null

        var id:Int? = alumnoVsTitulacion.id
        var codigoT:String? = alumnoVsTitulacion.codigoTitulacion
        var nifAlum:String? = alumnoVsTitulacion.NIFalumno



        try{
            statement= conn!!.createStatement()
            statement.executeUpdate("INSERT INTO \"public\".\"AlumnosVsTitulaciones\" VALUES ("+id+",'"+codigoT+"','"+nifAlum+"')")

        }catch (e:Exception){
            e.printStackTrace()
        }
    }

    fun insertarTitulacionesVsAsignaturas(titulacionVsAsignatura:TitulacionVsAsignatura){

        var statement:Statement? = null

        var id:Int? = titulacionVsAsignatura.id
        var codigoT:String? = titulacionVsAsignatura.codigoTitulacion
        var codigoAsig:String? = titulacionVsAsignatura.codigoAsignatura



        try{
            statement= conn!!.createStatement()
            statement.executeUpdate("INSERT INTO \"public\".\"TitulacionesVsAsignaturas\" VALUES ("+id+",'"+codigoT+"','"+codigoAsig+"')")

        }catch (e:Exception){
            e.printStackTrace()
        }
    }

}