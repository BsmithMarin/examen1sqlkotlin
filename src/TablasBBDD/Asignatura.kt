package TablasBBDD

class Asignatura {

    var codigo:String? = null
    var nombre:String? = null
    var creditos:Int? = null
    var profesor:String? = null

    constructor(Codigo:String,Nombre:String,Creditos:Int,Profesor:String){

        codigo=Codigo
        nombre=Nombre
        creditos=Creditos
        profesor=Profesor
    }

}