package TablasBBDD

class Titulacion {

    var codigo:String? = null
    var nombre:String? = null
    var creditos:Int? = null
    var precioCredito:Double? = null

    constructor(Codigo:String,Nombre:String,Creditos:Int,PrecioCredito:Double){

        codigo=Codigo
        nombre=Nombre
        creditos=Creditos
        precioCredito=PrecioCredito
    }
}