package TablasBBDD

class TitulacionVsAsignatura {

    var id:Int? = null
    var codigoTitulacion:String? = null
    var codigoAsignatura:String? = null

    constructor(ID:Int,CodigoTitulacion:String,CodigoAsignatura:String){

        id=ID
        codigoTitulacion=CodigoTitulacion
        codigoAsignatura=CodigoAsignatura

    }

}