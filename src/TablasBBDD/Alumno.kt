package TablasBBDD

import java.sql.Date

class Alumno {

    var nif:String? = null
    var nombre:String? = null
    var apellidos:String? = null
    var fechaNacimiento:Date? = null
    var discapacidad:Boolean? =null

    constructor(NIF:String,Nombre:String,Apellidos:String,FechaNacimiento:Date,Discapacidad:Boolean){
        nif=NIF
        nombre=Nombre
        apellidos=Apellidos
        fechaNacimiento=FechaNacimiento
        discapacidad=Discapacidad
    }
}