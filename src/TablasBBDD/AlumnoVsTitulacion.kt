package TablasBBDD

class AlumnoVsTitulacion {

    var id:Int? = null
    var codigoTitulacion:String? = null
    var NIFalumno:String? = null

    constructor(ID:Int,CodigoTitulacion:String,NIFAlumno:String){

        id=ID
        codigoTitulacion=CodigoTitulacion
        NIFalumno=NIFAlumno

    }
}